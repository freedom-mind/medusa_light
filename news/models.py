from django.db import models


class News(models.Model):
    class Meta:
        verbose_name = 'News'
        verbose_name_plural = 'News'

    created_date = models.DateField()
    title = models.CharField(max_length=255)
    content = models.TextField()

    def __str__(self):
        return self.title
