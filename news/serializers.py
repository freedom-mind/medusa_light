from rest_framework import serializers

from medusa_light.settings import REST_DATE_FORMAT
from .models import News


class NewsListSerializer(serializers.ModelSerializer):
    date = serializers.DateField(format=REST_DATE_FORMAT, source='created_date')
    subject = serializers.CharField(source='title')

    class Meta:
        model = News
        fields = ('date', 'subject', 'content')
