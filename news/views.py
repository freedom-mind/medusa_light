from rest_framework.response import Response
from rest_framework.views import APIView

from .models import News
from .serializers import NewsListSerializer


class NewsListView(APIView):
    """Вывод списка новостей"""

    def get(self, request):
        news = News.objects.order_by('-created_date')
        serializer = NewsListSerializer(news, many=True)
        return Response(serializer.data)
